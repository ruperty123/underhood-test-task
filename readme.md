## Candidate assessment test for "Underhood" company
`author: Ruslan Kulik`

## Setup guide:

### 1. Create and fill environment file
`cp .env.example .env`

### 2. Install docker and docker-compose
`https://docs.docker.com/engine/install/`

### 3. Build and run docker containers
`docker-compose up -d --build`

## Show result of task 1

### 1. Open app in web browser, using following url 
`http://localhost:8081`

## Show result of task 2
### 1. Enter into test task`s PHP container:
`docker exec -it underhood_test_task_php bash`

### 2. Execute search script inside PHP container:
`cd /var/www/html/console`
`php search.php --word Limited`