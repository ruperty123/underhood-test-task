<?php

namespace classes;

class App
{
    /**
     * app config
     * @var array
     */
    public readonly array $config;

    /**
     * app base path
     * @var string
     */
    public readonly string $root;

    /**
     * This class instance
     * @var App
     */
    protected static App $instance;

    public function __construct() {
        $root = $_SERVER['DOCUMENT_ROOT'];
        if (empty($root)) {
            // CLI has no DOCUMENT_ROOT
            $root = str_replace('/classes', '', __DIR__);
        }
        $this->root = $root;
        $this->config = require_once("{$this->root}/config.php");
    }

    /**
     * get http method params
     * @param string $name
     * @param string $method
     * @param string $type
     * @return string|int|bool
     */
    public function httpParams(string $name, string $method = 'GET', string $type = 'string'): string|int|bool {
        $source = ($method === 'GET' ? $_GET : $_POST);
        if (!isset($source[$name])) {
            return false;
        }
        if ($type === 'string') {
            return htmlspecialchars(trim($source[$name]));
        } else {
            return (int)$source[$name];
        }
    }

    /**
     * Class fast instance without duplication
     * @return App
     */
    public static function get(): App {
        if (!isset(self::$instance)) {
            self::$instance = new self;
        }
        return self::$instance;
    }

}