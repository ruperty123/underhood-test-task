<?php

namespace classes;

use mysqli;

class DB
{
    /**
     * mysql connect instance
     * @var mysqli
     */
    protected mysqli $mysqli;

    public function __construct() {
        $db_config = \classes\App::get()->config['db'];
        $this->mysqli = new mysqli($db_config['host'], $db_config['user'], $db_config['password'], $db_config['name']);
        if ($this->mysqli->connect_error) {
            throw new \Exception("Error connecting to database: {$this->mysqli->connect_error}", 500);
        }
    }

    /**
     * Prepare and execute SQL query safely
     * @param string $query
     * @param ...$params
     * @return array
     * @throws \Exception
     */
    public function exec(string $query, ...$params): array {
        $result = [];

        // prevent sql injections
        $statement = $this->mysqli->prepare($query);
        if (!$statement) {
            throw new \Exception("Error preparing sql query: {$this->mysqli->error}", 500);
        }

        if (!empty($params)) {
            $param_types = "";
            foreach ($params as $param) {
                if (is_numeric($param)) {
                    $param_types .= 'i';
                } else {
                    $param_types .= 's';
                }
            }
            $statement->bind_param($param_types, ...$params);
        }


        // execute query
        if (!$statement->execute()) {
            throw new \Exception("Error executing sql query: {$this->mysqli->error}", 500);
        }

        // get data
        $exec_result = $statement->get_result();
        if (is_bool($exec_result)) {
            return $result;
        }

        // fetch select results
        while ($row = $exec_result->fetch_assoc()) {
            $result[] = $row;
        }

        $statement->close();
        return $result;
    }
}