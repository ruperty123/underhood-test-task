<?php

namespace classes;
/**
 * Simple Curl wrapper
 */
class HttpDownload
{
    /**
     * CurlHandle instance
     * @var \CurlHandle|false
     */
    private \CurlHandle|false $curl;

    /**
     * Curl error
     * @var string
     */
    private string $error = "";

    public function __construct() {
        // Init curl connection
        $this->curl = curl_init();

        curl_setopt($this->curl, CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
        // save connection cookie
        curl_setopt($this->curl, CURLOPT_COOKIESESSION, true);
        curl_setopt($this->curl, CURLOPT_COOKIEJAR, 'saved-cookie');
        curl_setopt($this->curl, CURLOPT_COOKIEFILE, App::get()->root."/scrap_cookie");
    }


    /**
     *
     * @param string $url
     * @param string $method
     * @throws \Exception
     */
    public function init(string $url, string $method = "GET"): void
    {
        $this->error = "";
        // strict methods
        $method = strtoupper($method);
        if (!in_array($method, ['GET', 'POST'])) {
            throw new \Exception('Unsupported method');
        }

        // set curl options
        curl_setopt($this->curl, CURLOPT_URL, $url);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($this->curl, CURLOPT_HEADER, TRUE);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($this->curl, CURLOPT_FAILONERROR, true);

        if ($method === "POST") {
            curl_setopt($this->curl, CURLOPT_POST, TRUE);
        } else {
            curl_setopt($this->curl, CURLOPT_POST, FALSE);
        }
    }

    /**
     * Execute CURL request
     * @param array $data
     * @return string
     */
    public function send(array $data = []): string {
        // prevent any output
        ob_start();
        // add request params
        if (!empty($data)) {
            curl_setopt($this->curl, CURLOPT_POSTFIELDS, http_build_query($data));
        }
        // execute the curl command
        $response = curl_exec($this->curl);

        // stop preventing output
        $response = ob_end_clean().$response;

        // save error
        if (curl_errno($this->curl)) {
            $this->error = curl_error($this->curl);
        }

        // return content
        return $response;
    }

    /**
     * Get curl error
     * @return string
     */
    public function getError():string {
        return $this->error;
    }

    /**
     * @return void
     */
    public function close(): void
    {
        curl_close($this->curl);
    }
}