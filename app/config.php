<?php
    // app config params
    return [
        'debug' => true,
        'db' => [
            'host' => 'underhood_test_task_mysql',
            'port' => '3306',
            'name' => 'test_task_db',
            'user' => 'root',
            'password' => 'super_secret123',
        ],
    ];