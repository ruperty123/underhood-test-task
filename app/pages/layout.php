<?php
/**
 * @var string $page_content
 */
?>
<html lang="en-US">
<head>
    <title>Candidate assessment test for "Underhood" company</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/assets/style/main.css">
</head>
<body>
<header>
    <a href="/">Home</a>
</header>
<div class="page-wrapper"><?=$page_content?></div>
</body>
</html>