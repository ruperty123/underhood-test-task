<?php

// connect to DB and run query
use classes\DB;

$DB = new DB();

$query = "
    SELECT
        DATE_FORMAT(product_table.date, '%d.%m.%Y') as date_formatted,
        SUM(product_table.quantity * IFNULL(price_table.price, 0)) as products_price
    FROM `products` as product_table
    LEFT JOIN `price_log` as price_table ON price_table.product_id = product_table.product_id AND
                                            price_table.date = (
                                                SELECT MAX(price_inner_table.date)
                                                FROM `price_log` as price_inner_table
                                                WHERE price_inner_table.date <= product_table.date
                                                AND price_inner_table.product_id = product_table.product_id
                                            )
    GROUP BY product_table.date
    ORDER BY product_table.date
";

$warehouse_price_log = $DB->exec($query);
?>
<div class="result-table">
    <table class="users-table">
        <thead>
        <tr>
            <th>Date</th>
            <th>Total warehouse price</th>
        </tr>
        </thead>
        <?php if(empty($warehouse_price_log)):?>
            <tbody>
            <tr>
                <td colspan="2">No goods found</td>
            </tr>
            </tbody>
        <?php else:?>
            <tbody>
            <?php foreach($warehouse_price_log as $item):?>
                <tr>
                    <td><b><?=$item['date_formatted']?></b></td>
                    <td><?=number_format($item['products_price']) ?> $</td>
                </tr>
            <?php endforeach;?>
            </tbody>
        <?php endif;?>
    </table>
</div>
