<div class="block">
    <h1>Show result of task 2</h1>
    <h4>1. Enter into test task`s PHP container:</h4>
    <pre>docker exec -it underhood_test_task_php bash</pre>
    <br>
    <br>
    <h4>2. Execute search script inside PHP container:</h4>
    <pre>cd /var/www/html/console</pre>
    <pre>php search.php --word Limited</pre>
</div>
