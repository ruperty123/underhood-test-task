<?php
    // include init script
    require_once 'bootstrap.php';

    // all request goes throw here
    try {
        // Strict http methods
        if (!in_array($_SERVER['REQUEST_METHOD'], ['GET', 'POST', 'PUT', 'OPTION'])) {
            throw new Exception('Method not allowed', 405);
        }

        // Serving all other requests
        $page_name = 'pages/home.php';
        if (!empty($_GET['_page_name'])) {
            $page_name = "/pages/$_GET[_page_name].php";
        }
        $page_file_path = \classes\App::get()->root."/$page_name";
        unset($page_name);

        if (!file_exists($page_file_path)) {
            throw new Exception('Page not found', 404);
        }

        // render page
        ob_start();
        include $page_file_path;
        $page_content = ob_get_clean();

        // pass rendered page to default layout
        require_once \classes\App::get()->root."/pages/layout.php";
        exit;
    } catch (Exception $e) {
        // Prevent user from seeing error details
        if (!\classes\App::get()->config['debug'] && $e->getCode() >= 500) {
            echo "<h1>{$e->getCode()}: An error occured while processing your request, please try again later...</h1>";
            exit;
        }
        throw $e;
    }