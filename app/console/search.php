<?php

    // include init script
    require_once '../bootstrap.php';
    $HttpDownload = new \classes\HttpDownload();

    // retrieve console command args
    $options = getopt("w:", [
        'word:'
    ]);

    if (empty($options['word'])) {
        echo "[INFO] use -w flag to pass search word (php search.php --word Limited)\n";
        exit(0);
    }

    $word = trim($options['word']);

    echo "[INFO] Search by keyword '$word' started, please wait...\n";

    // retrieve CSRF token

    // Download casual page
    $HttpDownload->init('https://search.ipaustralia.gov.au/trademarks/search/advanced');
    $home_page_html = $HttpDownload->send();

    // check for http error
    if (!empty($HttpDownload->getError())) {
        echo "[ERROR] ".C_RED.$HttpDownload->getError().C_NC."\n";
        exit(250);
    }

    // search for csrf token
    if (!preg_match('/<meta\s+name="_csrf"\s+content="(.+)"/mi', $home_page_html, $matches)) {
        echo "[ERROR] ".C_RED."CSRF Token not parsed!".C_NC."\n";
        exit(250);
    }

    $csrf = $matches[1];
    unset($matches);

    // perform search request
    $HttpDownload->init('https://search.ipaustralia.gov.au/trademarks/search/doSearch', 'POST');
    $search_result_html = $HttpDownload->send([
        '_csrf' => $csrf,
        'wv[0]' => $word,
        'wt[0]' => 'PART',
        'weOp[0]' => 'AND',
        'wv[1]' => '',
        'wt[1]' => 'PART',
        'wrOp' => 'AND',
        'wv[2]' => '',
        'wt[2]' => 'PART',
        'weOp[1]' => 'AND',
        'wv[3]' => '',
        'wt[3]' => 'PART',
        'iv[0]' => '',
        'it[0]' => 'PART',
        'ieOp[0]' => 'AND',
        'iv[1]' => '',
        'it[1]' => 'PART',
        'irOp' => 'AND',
        'iv[2]' => '',
        'it[2]' => 'PART',
        'ieOp[1]' => 'AND',
        'iv[3]' => '',
        'it[3]' => 'PART',
        'wp' => '',
        '_sw' => 'on',
        'classList' => '',
        'ct' => 'A',
        'status' => '',
        'dateType' => 'LODGEMENT_DATE',
        'fromDate' => '',
        'toDate' => '',
        'ia' => '',
        'gsd' => '',
        'endo' => '',
        'nameField[0]' => 'OWNER',
        'name[0]' => '',
        'attorney' => '',
        'oAcn' => '',
        'idList' => '',
        'ir' => '',
        'publicationFromDate' => '',
        'publicationToDate' => '',
        'i' => '',
        'c' => '',
        'originalSegment' => '',
    ]);

    //echo $home_page_html;

    // check for http error
    if (!empty($HttpDownload->getError())) {
        echo "[ERROR] ".C_RED.$HttpDownload->getError().C_NC."\n";
        exit(250);
    }

    // get 302 redirected to location
    if (!preg_match('/\nlocation:\s(.+)\n/mi', $search_result_html, $matches)) {
        echo "[ERROR] ".C_RED."Error parsing location".C_NC."\n";
        exit(250);
    }
    $results_base_url = preg_replace('/\s+/', ' ', trim($matches[1]));
    unset($matches);

    echo "[INFO] 302 Redirect to $results_base_url\n\n";

    // filling results
    $total_results = [];

    // parse first page
    parseCompanies($total_results, $search_result_html);

    // if first page is empty - no point to paginate
    if (!empty($total_results)) {
        // detect pages count
        $pages_count = 1;
        if (preg_match('/goto-last-page.+\n?data-gotopage="(\d+)"/i', $search_result_html, $matches)) {
            $pages_count = (int)$matches[1];
        }
        unset($matches);

        // pagination
        if ($pages_count > 1) {
            $page = 1;
            while ($page <= $pages_count) {
                // download next page
                $next_page_url = $results_base_url.'&p='.$page;

                echo "[INFO] Parsing page $page... $next_page_url\n";
                $HttpDownload->init($next_page_url);
                $current_page_html = $HttpDownload->send();

                // parse next page html to global pull
                parseCompanies($total_results, $current_page_html);

                // increment new page
                $page ++;
            }
        }
    }

    echo json_encode($total_results, JSON_PRETTY_PRINT)."\n";

    echo "[INFO] Found ".count($total_results)." companies total.\n";
    $HttpDownload->close();
    exit(0);

    /**
     * Parse one page html
     *
     * @param array $result
     * @param string $html
     * @return void
     * @throws Exception
     */
    function parseCompanies(array &$result, string $html): void {
        // parse all numbers
        if (!preg_match_all('/<a\s+href="\/trademarks\/search\/view.+>([\d-]+)<\/a>/im', $html, $matches)) {
            return;
        }
        $numbers = $matches[1];

        // parse all names
        if (!preg_match_all('/class="trademark words".+\n(.+)\n/im', $html, $matches)) {
            throw new Exception('Error parsing names');
        }
        $names = $matches[1];

        // parse all classes
        if (!preg_match_all('/<td\s+class="\s?classes\s?">\n?\s?(.+)/mi', $html, $matches)) {
            throw new Exception('Error parsing classes');
        }
        $classes = $matches[1];

        // parse all anchors
        if (!preg_match_all('/<a\s+href="(\/trademarks\/search\/view\/.+)">/mi', $html, $matches)) {
            throw new Exception('Error parsing detailed pages');
        }
        $url_details_pages = $matches[1];

        // parse all statuses
        if (!preg_match_all('/<td\s+class="status">\n?(<.+>)?\n?<.+>\n?(<span>)?\n?(.+)\n?(<\/span>)?/mi', $html, $matches)) {
            throw new Exception('Error parsing statuses');
        }
        $statuses = $matches[3];


        unset($matches);

        // combine all data together
        if (
            count($numbers) !== count($names) ||
            count($numbers) !== count($classes) ||
            count($numbers) !== count($url_details_pages) ||
            count($numbers) !== count($statuses)
        ) {
            throw new Exception('Parsed columns missmatch count');
        }
        foreach ($numbers as $index => $number) {
            $url_logo = '';

            // try parse logo
            if (preg_match('/<td\s+class="trademark\s+image" >\n?<img.+\n?src="(.+'.$number.'\/TRADE_MARK.+)"/mi', $html, $matches)) {
                $url_logo = $matches[1];
            }

            // combine final view
            $result[] = [
                'number' => $number,
                'name' => $names[$index],
                'class' => $classes[$index],
                'status' => $statuses[$index],
                'url_details_pages' => 'https://search.ipaustralia.gov.au'.explode('"', $url_details_pages[$index])[0],
                'url_logo' => $url_logo,
            ];
        }
    }