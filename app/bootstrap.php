<?php
    CONST C_RED='\033[0;31m'; # red color
    CONST C_NC='\033[0m'; # No Color

    // class autoload
    spl_autoload_register(function ($class_name) {
        $root = $_SERVER['DOCUMENT_ROOT'];
        if (empty($root)) {
            // CLI has no DOCUMENT_ROOT
            $root = str_replace('/console', '', __DIR__);
        }
        require_once $root."/".str_replace('\\', '/', $class_name).".php";
    });

    // error output
    if (\classes\App::get()->config['debug']) {
        ini_set('display_errors', '1');
        ini_set('display_startup_errors', '1');
        error_reporting(E_ALL);
    } else {
        ini_set('display_errors', '0');
        ini_set('display_startup_errors', '0');
        error_reporting(0);
    }

